# -*- coding: utf-8 -*-
import os, sys
import processoExec as task

print("""
####################################
## TESTANDO API RESTFUL com FLASK ##
####################################

""")

ip = input('>>> Digite o IP da aplicação com pontos (Ex: 0.0.0.0): ')
processo = input('>>> Digite o programa em execução: ')

token = task.gerarToken(ip)
print(token)

task.iniciado(processo, ip)

pergunta = str(input('O programa está fechado? (S / N) : '))

if pergunta.upper() == 'S':
	task.encerrado(processo, ip)
	print('O processo foi encerrado.')
else:
	exit()