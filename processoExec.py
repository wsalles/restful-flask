# -*- coding: utf-8 -*-
import json, requests, sys

def gerarToken(ip):
	#Shell Script
	#apt install jq
	#yum install jq
	#curl --header "Content-Type: application/json" -X POST --data '{"username" : "wsalles","password" : "Globo123"}' http://ip_da_aplicacao:3333/auth| jq '.access_token' -r
	url = 'http://' + ip + ':3333/auth'
	dados = {"username" : "wsalles","password" : "Globo123"}
	header = {"Content-Type": "application/json"}
	resposta = requests.post(url, data=json.dumps(dados), headers=header)
	meuToken = resposta.json()
	return meuToken['access_token']

def iniciado(processo, ip):
	#Shell Script
	#curl -X PUT http://ip_da_aplicacao:3333/processes/rsync_4K_HDR -H "authorization: JWT $token" -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"working" : "true"}'
	url = 'http://' + ip + ':3333/processes/' + processo
	token = gerarToken()
	dados = {'working' : "true", 'extras1' : sys.argv[1], 'extras2' : sys.argv[2]}
	header = {'Authorization' : 'JWT ' + token, "Content-Type": "application/json"}
	resposta = requests.put(url, data=json.dumps(dados), headers=header)

def encerrado(processo, ip):
	#Shell Script
	#curl -X PUT http://ip_da_aplicacao:3333/processes/rsync_4K_HDR -H "authorization: JWT $token" -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"working" : ""}'
	url = 'http://' + ip + ':3333/processes/' + processo
	token = gerarToken()
	dados = {'working' : '', 'extras1' : sys.argv[1], 'extras2' : sys.argv[2]}
	header = {'Authorization' : 'JWT ' + token, "Content-Type": "application/json"}
	resposta = requests.put(url, data=json.dumps(dados), headers=header)