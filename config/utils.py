# -*- coding: utf-8 -*-
import time, datetime, os
import subprocess as sub

def tempoReal():
	tempo = time.time()
	tempoData = datetime.datetime.fromtimestamp(tempo).strftime('%d/%m/%Y %H:%M:%S')
	return tempoData