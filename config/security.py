# -*- coding: utf-8 -*-
from werkzeug.security import safe_str_cmp
from config.user import Usuario

users = [
    Usuario(1, 'wsalles', 'Globo123'),
    Usuario(2, 'suporte', 'Globo123'),
]

username_table = {u.username: u for u in users}
userid_table = {u.id: u for u in users}

def autenticacao(username, password):
    user = username_table.get(username, None)
    if user and safe_str_cmp(user.password, password):
        return user

def identificacao(payload):
    user_id = payload['identity']
    return userid_table.get(user_id, None)