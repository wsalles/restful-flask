# Instalação do Python 3.7
cd /usr/src/

wget https://www.python.org/ftp/python/3.7.0/Python-3.7.0.tgz

tar xzf Python-3.7.0.tgz

rm -f Python-3.7.0.tgz

cd Python-3.7.0/

./configure --enable-optimizations

make altinstall

python3.7 -V


### Criar ambiente virtualenv
python3 -m venv processes
### Ativar o ambiente (apenas um exemplo abaixo)
. /opt/processes/bin/activate

# Dependencias necessárias
Flask

Flask-JWT

Flask-RESTful

requests

## Instalar dependencias
pip install --upgrade pip

pip install flask

pip install Flask-JWT

pip install Flask-RESTful


# Criar um serviço no CentOS
cd /etc/systemd/system

vim script-manager-api.service

### Content file


[Unit]

Description = Script Manager

After = network.target

[Service]

ExecStart = /usr/bin/script-manager-api

[Install]

WantedBy = multi-user.target

systemctl enable script-manager-api.service

systemctl start script-manager-api.service
