# -*- coding: utf-8 -*-
from flask import Flask, request
from flask_restful import Resource, Api, reqparse
from flask_jwt import JWT, jwt_required
from config.security import autenticacao, identificacao
import config.utils as util
import sys, os

app = Flask(__name__)
app.secret_key = 'Wallace Salles'
api = Api(app)
jwt = JWT(app, autenticacao, identificacao)

processos = []

class Processo(Resource):

	parser = reqparse.RequestParser()
	parser.add_argument('working', type=bool, required=True, help="Esse campo não pode ficar em branco")
	parser.add_argument('extras1', type=str, required=True, help="Esse campo pode ficar em branco")
	parser.add_argument('extras2', type=str, required=True, help="Esse campo pode ficar em branco")
		
	def get(self, nome):
		processo = next(filter(lambda x: x['process'] == nome, processos), None)
		return {'process' : processo}, 200 if processo else 404

	@jwt_required()
	def post(self, nome):
		if next(filter(lambda x: x['process'] == nome, processos), None):
			return {'mensagem' : "O item {} não existe.".format(nome)}, 404
		data = request.get_json()
		processo = {'process' : nome, 'working' : data['working'], 'lastUpdate' : util.tempoReal()}
		processos.append(processo)
		return processo, 201

	@jwt_required()
	def delete(self, nome):
		global processos
		processos = list(filter(lambda x: x['process'] != nome, processos))
		return {'mensagem' : 'O processo foi deletado da lista.'}

	@jwt_required()
	def put(self, nome):
		data = Processo.parser.parse_args()
		print(data)
		processo = next(filter(lambda x: x['process'] == nome, processos), None)
		if processo is None:
			processo = {'process' : nome, 'working' : data['working'], 'lastUpdate' : util.tempoReal(),
						'extras1' : data['extras1'], 'extras2' : data['extras2']}
			processos.append(processo)
		else:
			processo.update(data)			
			processo.update({'lastUpdate' : util.tempoReal()})
		return processo

class ListaProcessos(Resource):
	def get(self):
		return {'scripts' : processos}

api.add_resource(Processo, '/processes/<string:nome>')
api.add_resource(ListaProcessos, '/processes')


app.run(debug=True, host='0.0.0.0', port=3333)